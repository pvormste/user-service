package main

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/proskive/microservices/user-service/config"
	"gopkg.in/resty.v1"
	"net"
	"strconv"
	"strings"
	"time"
)

const (
	status_up string = "UP"
	status_down string = "DOWN"
	status_starting string = "STARTING"
	status_out_of_service string = "OUT_OF_SERVICE"
	status_unknown string = "UNKNOWN"
)

type EurekaResource struct {
	Instance *eurekaInstanceResource `json:"instance"`
}

type eurekaInstanceResource struct {
	Hostname string `json:"hostName"`
	App string `json:"app"`
	IpAddr string `json:"ipAddr"`
	VipAddr string `json:"vipAddress"`
	SecureVipAddr string `json:"secureVipAddress"`
	Status string `json:"status"`
	Port *eurekaPort `json:"port"`
	SecurePort *eurekaPort `json:"securePort"`
	HomePageUrl string `json:"homePageUrl"`
	StatusPageUrl string `json:"statusPageUrl"`
	HealthCheckUrl string `json:"healthCheckUrl"`
	DataCenterInfo *eurekaDataCenterInfoResource `json:"dataCenterInfo"`
	Metadata *eurekaMetadataResource `json:"metadata"`
}

type eurekaPort struct {
	Number string `json:"$"`
	Enabled string `json:"@enabled"`
}

type eurekaDataCenterInfoResource struct {
	Class string `json:"@class"`
	Name string `json:"name"`
}

type eurekaMetadataResource struct {
	InstanceId string `json:"instanceId"`
}

func RegisterWithEureka() error {
	// Get IP
	if config.Get().Instance.Ip == "" {
		config.Get().Instance.Ip = getLocalIP()
	}

	// Check if SSL Port is enabled
	var sslPortEnabled = "false"
	if config.Get().Instance.SslPort != 0 {
		sslPortEnabled = "true"
	}

	// Create Eureka Resource
	eurekaResource := EurekaResource{
		Instance: &eurekaInstanceResource{
			Hostname: config.Get().Instance.Hostname,
			App: strings.ToUpper(config.Get().Instance.Name),
			IpAddr: config.Get().Instance.Ip,
			VipAddr: config.Get().Instance.Vip,
			SecureVipAddr: config.Get().Instance.Ip,
			Status: status_up,
			Port: &eurekaPort{
				Number: strconv.Itoa(config.Get().Instance.Port),
				Enabled: "true",
			},
			SecurePort: &eurekaPort{
				Number: strconv.Itoa(config.Get().Instance.SslPort),
				Enabled: sslPortEnabled,
			},
			HomePageUrl: fmt.Sprintf("http://%v:%v/", config.Get().Instance.Hostname, config.Get().Instance.Port),
			StatusPageUrl: fmt.Sprintf("http://%v:%v/info", config.Get().Instance.Hostname, config.Get().Instance.Port),
			HealthCheckUrl: fmt.Sprintf("http://%v:%v/health", config.Get().Instance.Hostname, config.Get().Instance.Port),
			DataCenterInfo: &eurekaDataCenterInfoResource{
				Class: "com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo",
				Name: config.Get().Instance.DataCenterName,
			},
			Metadata: &eurekaMetadataResource{
				InstanceId: config.Get().Instance.Id,
			},
		},
	}

	// Http Client
	resp, err := resty.R().
		SetHeader("Content-Type", "application/json").
		SetBody(eurekaResource).
		SetPathParams(map[string]string{
			"appId": config.Get().Instance.Name,
		}).
		Post(eurekaPath("/apps/{appId}"))

	if err != nil {
		log.Error().Err(err).Msg("Could not register with Eureka")
		return err
	}

	if resp.StatusCode() != 204 {
		log.Error().Str("URL", resp.Request.URL).
			Int("Status Code", resp.StatusCode()).
			Str("Response Body", resp.String()).
			Msg("Eureka returned an invalid status code")
		return err
	}

	// glg.Successf("Successfully registered with Eureka (%v) with IP %v! Starting heartbeat ...", resp.Request.URL, c.Instance.Ip)
	log.Info().Str("URL", resp.Request.URL).Str("IP", config.Get().Instance.Ip).Msg("Successfully registered with Eureka!")
	return nil
}

func sendHeartbeatToEureka() {
	for {
		time.Sleep(30 * time.Second)

		// glg.Debugf("Sending heartbeat ...")
		log.Debug().Msg("Sending heartbeat ...")
		resp, err := resty.R().
			SetPathParams(map[string]string{
				"appId": config.Get().Instance.Name,
				"hostname": config.Get().Instance.Hostname,
				"instanceId": config.Get().Instance.Id,
			}).
			Put(eurekaPath("/apps/{appId}/{hostname}:{instanceId}"))

		if err != nil {
			// glg.Failf("Could not send heartbeat: %v", err)
			log.Error().Err(err).Msg("Could not send heartbeat")
			continue
		}

		if resp.StatusCode() != 200 {
			// glg.Failf("Eureka (%v) returned an invalid status code for heartbeat: %v", resp.Request.URL, resp.StatusCode())
			log.Error().Str("URL", resp.Request.URL).Int("Status Code", resp.StatusCode()).Msg("Eureka returned an invalid status code for heartbeat")

			if resp.StatusCode() == 404 {
				// glg.Infof("Trying to re-register this service ...")
				log.Info().Msg("Trying to re-register this service ...")
				RegisterWithEureka()
			}
		}
	}
}

func deregisterFromEureka()  {
	// glg.Infof("De-registering from Eureka ...")
	log.Info().Msg("De-registering from Eureka ...")
	resp, err := resty.R().
		SetPathParams(map[string]string{
			"appId": config.Get().Instance.Name,
			"hostname": config.Get().Instance.Hostname,
			"instanceId": config.Get().Instance.Id,
		}).
		Delete(eurekaPath("/apps/{appId}/{hostname}:{instanceId}"))

	if err != nil {
		// glg.Failf("Could not de-register from Eureka: %v\n", err)
		log.Error().Err(err).Msg("Could not de-register from Eureka")
	}

	if resp.StatusCode() != 200 {
		// glg.Failf("Eureka (%v) returned an invalid status code for de-registering: %v", resp.Request.URL, resp.StatusCode())
		log.Error().
			Str("URL", resp.Request.URL).
			Int("Status Code", resp.StatusCode()).
			Msg("Eureka returned an invalid status code for de-registering")
	}
}

func eurekaPath(path string) string {
	return fmt.Sprintf("%v%v", config.Get().Eureka.Url, path)
}

func getLocalIP() string {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		// log.Println(err)
		log.Error().Err(err).Msg("Could not find out IP address. Setting to 127.0.0.1")
		return "127.0.0.1"
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)
	return localAddr.IP.String()
}