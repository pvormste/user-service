package config

type Config struct {
	Adapter string
	Keycloak    *KeycloakConfig
	Eureka      *EurekaConfig
	Instance    *InstanceConfig
	Loglevel    string
}

type KeycloakConfig struct {
	Url    string
	Realm  string
	Client string
	Secret string
}

type EurekaConfig struct {
	Url string
}

type InstanceConfig struct {
	Id             string
	Name           string
	Hostname       string
	Ip             string
	Vip            string
	Port           int
	SslPort        int
	DataCenterName string
}

var conf *Config

func Get() *Config {
	if conf == nil {
		conf = &Config{}
	}

	return conf
}

func Set(c *Config) {
	conf = c
}
