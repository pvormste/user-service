package controller

import (
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/proskive/microservices/user-service/keycloak"
	"gopkg.in/resty.v1"
	"net/http"
)

func (k *KeycloakController) GetAllRoles(w http.ResponseWriter, r *http.Request)  {
	// Get token for rest access
	token, err := keycloak.GetRestToken()
	if err != nil {
		keycloak.LogRequestError("Error when trying to retrieve rest token for Keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Compose endpoint
	url, err := keycloak.RestUrl("/roles")
	if err != nil {
		keycloak.LogRequestError("Could not compose rest url for keycloak", r, err)
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
		return
	}

	// Do the request
	resp, err := resty.R().
		SetAuthToken(token).
		Get(url)

	if err != nil {
		log.Error().Str("Rest URL", url).
			Err(err).
			Str("Request URI", r.RequestURI).
			Msg("Could not execute request to rest api")
		ErrorResponse(w, r, http.StatusInternalServerError, "An internal server error occured")
	}

	// Convert response
	var keycloakRoles []keycloak.RoleResource
	json.Unmarshal(resp.Body(), &keycloakRoles)
	proskiveRoles := keycloak.MultipleRolesToProSkive(keycloakRoles)

	// Write response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(proskiveRoles)
}

func RoleByName(roleName string) (*keycloak.RoleResource, error) {
	if roleName == "" {
		return &keycloak.RoleResource{}, errors.New("Role name should not be empty")
	}

	// Get token for rest access
	token, err := keycloak.GetRestToken()
	if err != nil {
		return &keycloak.RoleResource{}, err
	}

	// Compose endpoint
	url, err := keycloak.RestUrl("/roles/{roleName}")
	if err != nil {
		return &keycloak.RoleResource{}, err
	}

	// Do request
	resp, err := resty.R().
		SetAuthToken(token).
		SetPathParams(map[string]string{
			"roleName": roleName,
		}).
		Get(url)

	if err != nil {
		return &keycloak.RoleResource{}, err
	}

	if resp.StatusCode() != http.StatusOK {
		return &keycloak.RoleResource{}, errors.New("Role not found")
	}

	var role *keycloak.RoleResource
	err = json.Unmarshal(resp.Body(), role)
	if err != nil {
		return &keycloak.RoleResource{}, err
	}

	return role, nil
}