package controller

import "net/http"

type UserController interface {
	GetAllUsers(w http.ResponseWriter, r *http.Request)
	GetUser(w http.ResponseWriter, r *http.Request)
	AddUser(w http.ResponseWriter, r *http.Request)
	UpdateUser(w http.ResponseWriter, r *http.Request)
	DeleteUser(w http.ResponseWriter, r *http.Request)
	GetRolesFromUser(w http.ResponseWriter, r *http.Request)
	AddRoleToUser(w http.ResponseWriter, r *http.Request)
}

type RoleController interface {
	GetAllRoles(w http.ResponseWriter, r *http.Request)
}

type GroupController interface {
	GetAllGroups(w http.ResponseWriter, r *http.Request)
}

type ApiController interface {
	UserController
	RoleController
	GroupController
}