package keycloak

type TokenResource struct {
	AccessToken      string `json:"access_token"`
	ExpiresIn        int    `json:"expires_in"`
	RefreshExpiresIn int    `json:"refresh_expires_in"`
	RefreshToken     string `json:"refresh_token"`
	TokenType        string `json:"token_type"`
	NotBeforePolicy  int    `json:"not-before-policy"`
	SessionState     int    `json:"session_state"`
	Scope            string `json:"scope"`
}

type UserResource struct {
	ID               string                 `json:"id"`
	CreatedTimestamp int                    `json:"createdTimestamp"`
	Username         string                 `json:"username"`
	Enabled          bool                   `json:"enabled"`
	TOTP             bool                   `json:"totp"`
	EmailVerified    bool                   `json:"emailVerified"`
	FirstName        string                 `json:"firstName"`
	LastName         string                 `json:"lastName"`
	Email            string                 `json:"email"`
	Attributes       *UserAttributeResource `json:"attributes"`
	NotBefore        int                    `json:"notBefore"`
}

type UserAttributeResource struct {
	Phone              []string `json:"phone"`
	Street             []string `json:"street"`
	Phone2             []string `json:"phone2"`
	Mobile             []string `json:"mobile"`
	Institute          []string `json:"institute"`
	Clinic             []string `json:"clinic"`
	Title              []string `json:"title"`
	Fax                []string `json:"fax"`
	Building           []string `json:"building"`
	Room               []string `json:"room"`
	TermsAndConditions []string `json:"terms_and_conditions"`
	Locale             []string `json:"locale"`
}

type UserAccessResource struct {
	ManageGroupMembership bool `json:"manageGroupMembership"`
	View                  bool `json:"view"`
	MapRoles              bool `json:"mapRoles"`
	Impersonate           bool `json:"impersonate"`
	Manage                bool `json:"manage"`
}

type RoleResource struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Composite   bool   `json:"composite"`
	ClientRole  bool   `json:"clientRole"`
	ContainerID string `json:"containerId"`
}

type GroupResource struct {
	ID         string          `json:"id"`
	Name       string          `json:"name"`
	Path       string          `json:"path"`
	RealmRoles []string        `json:"realmRoles"`
	SubGroups  []GroupResource `json:"subGroups"`
}
