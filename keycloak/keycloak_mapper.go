package keycloak

import "gitlab.com/proskive/microservices/user-service/proskive"

func HasAttribute(attr []string) bool {
	if len(attr) > 0 {
		return true
	}
	return false
}

func GetAttribute(attr []string) string {
	if HasAttribute(attr) {
		return attr[0]
	}
	return ""
}

func SetAttribute(attr string) []string {
	if attr == "" {
		return nil
	}
	return append([]string{}, attr)
}

func UserToProSkive(ku *UserResource) *proskive.UserResource {
	return &proskive.UserResource{
		ID: ku.ID,
		Username: ku.Username,
		Title: GetAttribute(ku.Attributes.Title),
		FirstName: ku.FirstName,
		LastName: ku.LastName,
		Clinic: GetAttribute(ku.Attributes.Clinic),
		Institute: GetAttribute(ku.Attributes.Institute),
		Street: GetAttribute(ku.Attributes.Street),
		Building: GetAttribute(ku.Attributes.Building),
		Room: GetAttribute(ku.Attributes.Room),
		Email: ku.Email,
		Phone: GetAttribute(ku.Attributes.Phone),
		Phone2: GetAttribute(ku.Attributes.Phone2),
		Mobile: GetAttribute(ku.Attributes.Mobile),
		Fax: GetAttribute(ku.Attributes.Fax),
		PolicyAccepted: HasAttribute(ku.Attributes.TermsAndConditions),
		Enabled: ku.Enabled,
		EmailVerified: ku.EmailVerified,
		Locale: GetAttribute(ku.Attributes.Locale),
	}
}

func MultipleUsersToProSkive(kusers []UserResource) []*proskive.UserResource {
	var pusers []*proskive.UserResource
	for _, u := range kusers {
		pusers = append(pusers, UserToProSkive(&u))
	}

	return pusers
}

func UserToKeycloak(u *proskive.UserResource, id string) *UserResource {
	return &UserResource{
		ID: id,
		Username: u.Username,
		FirstName: u.FirstName,
		LastName: u.LastName,
		Email: u.Email,
		Enabled: u.Enabled,
		EmailVerified: u.EmailVerified,
		Attributes: &UserAttributeResource{
			Title: SetAttribute(u.Title),
			Clinic: SetAttribute(u.Clinic),
			Institute: SetAttribute(u.Institute),
			Street: SetAttribute(u.Street),
			Building: SetAttribute(u.Building),
			Room: SetAttribute(u.Room),
			Phone: SetAttribute(u.Phone),
			Phone2: SetAttribute(u.Phone2),
			Mobile: SetAttribute(u.Mobile),
			Fax: SetAttribute(u.Fax),
			Locale: SetAttribute(u.Locale),
			TermsAndConditions: nil,
		},
	}
}

func RoleToProSkive(kr *RoleResource) *proskive.RoleResource {
	return &proskive.RoleResource{
		ID: kr.ID,
		Name: kr.Name,
		Description: kr.Description,
	}
}

func MultipleRolesToProSkive(kr []RoleResource) []*proskive.RoleResource {
	var proles []*proskive.RoleResource
	for _, r := range kr {
		proles = append(proles, RoleToProSkive(&r))
	}

	return proles
}

func GroupToProSkive(kg *GroupResource) proskive.GroupResource {
	return proskive.GroupResource{
		ID: kg.ID,
		Name: kg.Name,
		Path: kg.Path,
		Roles: kg.RealmRoles,
		SubGroups: MultipleGroupsToProSkive(kg.SubGroups),
	}
}

func MultipleGroupsToProSkive(kg []GroupResource) []proskive.GroupResource {
	var pgroups []proskive.GroupResource
	for _, g := range kg {
		pgroups = append(pgroups, GroupToProSkive(&g))
	}

	return pgroups
}