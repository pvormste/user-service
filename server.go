package main

import (
	"fmt"
	"github.com/go-chi/chi"
	"github.com/rs/zerolog/log"
	"gitlab.com/proskive/microservices/user-service/config"
	"gitlab.com/proskive/microservices/user-service/controller"
	"net/http"
)

func startServer(ctrl controller.ApiController)  {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/users", func(r chi.Router) {
			r.Get("/", ctrl.GetAllUsers)
			r.Get("/{userId}", ctrl.GetUser)
			r.Post("/", ctrl.AddUser)
			r.Put("/{userId}", ctrl.UpdateUser)
			r.Delete("/{userId}", ctrl.DeleteUser)
			r.Get("/{userId}/roles", ctrl.GetRolesFromUser)
			r.Put("/{userId}/role/{roleName}", ctrl.AddRoleToUser)
		})

		r.Route("/roles", func(r chi.Router) {
			r.Get("/", ctrl.GetAllRoles)
		})

		r.Route("/groups", func(r chi.Router) {
			r.Get("/", ctrl.GetAllGroups)
		})
	})

	log.Info().Msgf("Starting %v server on port :%v ...", config.Get().Instance.Name, config.Get().Instance.Port)
	http.ListenAndServe(fmt.Sprintf(":%v", config.Get().Instance.Port), r)
}