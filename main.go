package main

import (
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gitlab.com/proskive/microservices/user-service/config"
	"gitlab.com/proskive/microservices/user-service/controller"
	"gopkg.in/resty.v1"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

const (
	adapter_keycloak string = "keycloak"
)

func main() {
	// Load config
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.Panic().Err(err).Msg("Error when reading config file")
		panic(err)
	}
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
	viper.Unmarshal(config.Get())

	// Logging
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	switch config.Get().Loglevel {
	case "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	default:
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
		resty.SetDisableWarn(true)
	}

	// Register with Eureka if url is set
	if config.Get().Eureka != nil && config.Get().Eureka.Url != "" {
		// Set instance id first (if not already set)
		if config.Get().Instance.Id == "" {
			config.Get().Instance.Id = fmt.Sprintf("%v", config.Get().Instance.Port)
		}

		// Now register
		err := RegisterWithEureka()

		// Handle de-registering from Eureka
		handleSigterm()

		// Send heartbeat
		if err == nil {
			go sendHeartbeatToEureka()
		}
	}

	// Select adapter
	var ctrl controller.ApiController
	switch strings.ToLower(config.Get().Adapter) {
	case adapter_keycloak:
		ctrl = &controller.KeycloakController{}
	default:
		ctrl = &controller.KeycloakController{}
	}

	// Start server
	startServer(ctrl)
}


func handleSigterm()  {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-c
		deregisterFromEureka()
		os.Exit(0)
	}()
}





